# 连接器
连接器 是一个允许从外部源编辑文本文档、电子表格、演示文稿和可填写表单的类

结合 `BUILDER.API` 能发挥更强大的能力

## 自定义右键菜单
<Card src='/connector/right-menu.gif' desc="自定义右键菜单" price="299.00"/>

## 光标坐标&被选中的文字
<Card src='/connector/cursor-points.gif' desc="光标坐标&被选中的文字" price="299.00"/>

## 水印
<Card src='/connector/watermark.gif' desc="水印" price="199.00"/>

## 表格操作
<Card src='/connector/table.gif' desc="表格操作" price="299.00"/>

## 书签操作
<Card src='/connector/bookmark.gif' desc="书签操作" price="299.00"/>

## 内容控件操作
<Card src='/connector/content-controls.gif' desc="内容控件操作" price="299.00"/>

## 无刷新切换`编辑/预览`状态
<Card src='/connector/set-mode.gif' desc="无刷新切换编辑/预览状态" price="99.00"/>

## 获取文件链接
<Card src='/connector/download-url.gif' desc="获取文件链接" price="99.00"/>

## 更多更能开发中



<script setup>
import Footer from '../../components/Footer.vue'
import Card from '../../components/Card.vue'
</script>

<Footer tip=" "/>
