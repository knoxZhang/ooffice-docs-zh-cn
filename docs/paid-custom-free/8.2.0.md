# 免费镜像

## base 镜像 （<span style="color:red">￥ 0.00</span>）


### 包含功能

1. 去除 20 链接数限制
   ![online-users](/pay/online-users-8.2.0.jpg)
   <a href="../pay/online-users-8.2.0.jpg" target="_blank">点击放大</a>


## 下载

### docker
```sh
# 拉取
docker pull knoxzhang/oo-ce-docker-license:8.2.0.1
# 启动
docker run --name=ooffice -i -t -d -p 8080:80 --restart=always knoxzhang/oo-ce-docker-license:8.2.0.1
```
### 百度网盘

```sh
链接: https://pan.baidu.com/s/1wfUHVDrM1fablcFV301W3Q 提取码: avvp 复制这段内容后打开百度网盘手机App，操作更方便哦

```

<a style="font-size:12px" href="https://hub.docker.com/r/knoxzhang/oo-ce-docker-license" target="_blank">去下载</a>

## 联系方式

QQ：601424688

<script setup>
import Footer from '../components/Footer.vue'
</script>

<Footer tip=" "/>
